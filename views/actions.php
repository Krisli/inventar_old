<?php
/**
 * Created by PhpStorm.
 * User: Niku
 * Date: 9/13/2018
 * Time: 1:53 PM
 */

require_once SITE_ROOT . DS . "entities" . DS . "actions.php";

$entity = new ActionsEntity();
$actions  = $entity->getActions();

if (isset($_GET['task'])) {
    if ($_GET['task'] == 'save') {
        $entity->saveAction($_POST);
    } else if ($_GET['task']  == 'delete') {
        $entity->deleteAction($_GET);
    }
}
?>

<div class="container">

    <div class="header-buttons">
        <a href="?view=action" class="btn btn-outline-success float-left">Shto</a>
    </div>

    <table class="table table-responsive">

    <thead>
        <th>
            #
        </th>
        <th>
            Shitjet
        </th>
        <th>
            Furnizimet
        </th>
        <th>
            Data
        </th>
        <th>
            Produkti
        </th>
        <th width="5%">

        </th>
        <th width="5%">

        </th>
    </thead>

    <tbody>
	<?php
    $count=1;
    foreach ($actions as $action) { ?>
        <tr>
            <td><?php echo $count ?></td>
            <td><?php echo !empty($action->outgoing) ? $action->outgoing : '-' ?></td>
            <td><?php echo !empty($action->incoming) ? $action->incoming : '-' ?></td>
            <td><?php echo Util::formatDate($action->date) ?></td>
            <td><?php echo $action->item_name ?></td>

            <td>
                <a href="?view=action&id=<?php echo $action->id ?>" class="btn btn-outline-primary">Modifiko</a>
            </td>
            <td>
                <a href="?view=actions&task=delete&id=<?php echo $action->id ?>" class="btn btn-outline-danger">Fshi</a>
            </td>
        </tr>
	<?php
	$count++;
	} ?>
    </tbody>
</table>
