<?php
/**
 * Created by PhpStorm.
 * User: Niku
 * Date: 9/12/2018
 * Time: 1:58 PM
 */

require_once SITE_ROOT . DS . "entities" . DS . "items.php";
require_once SITE_ROOT . DS . "entities" . DS . "units.php";

$entity2 = new UnitsEntity();
$units  = $entity2->getUnits();

$item = null;
if (isset($_GET['id'])) {
    $entity = new ItemsEntity();
    $item = $entity->getItem($_GET['id']);
}

$name = !empty($item) ? $item->name : '';
$description = !empty($item) ? $item->description : '';
$id   = !empty($item) ? $item->id : 0;
?>

<div class="container">
    <form method="post" action="?view=items&task=save">
        <div class="form-group">
            <label for="name">Emer     </label>
            <input type="text" id="name" name="name" placeholder="Emer" value="<?php echo $name ?>"/>
        </div>

        <label for="description">Pershkrimi</label>
        <div class="form-group">
            <textarea name="description" id="description" cols="30" rows="10">
                <?php echo $description; ?>
            </textarea>
        </div>

        <div class="form-group">
            <label for="unit_id">Njesia</label>
            <select name="unit_id" id="unit_id">
                <?php
                    foreach ($units as $unit) { ?>
                        <option value="<?php echo $unit->id; ?>"><?php echo $unit->name; ?></option>
                <?php } ?>
            </select>
        </div>

        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
        <button type="submit" class="btn btn-outline-success">Ruaj</button>
        <a href="?view=items" class="btn btn-outline-warning">Anullo</a>
    </form>
</div>