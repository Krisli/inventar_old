<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 18-09-12
 * Time: 5.30.MD
 */

require_once SITE_ROOT . DS . "entities" . DS . "units.php";

$entity = new UnitsEntity();
$units  = $entity->getUnits();

if (isset($_GET['task'])) {
    if ($_GET['task'] == 'save') {
        $entity->saveUnit($_POST);
    } else if ($_GET['task']  == 'delete') {
	    $entity->deleteUnit($_GET);
    }
}
?>

<div class="container">

    <div class="header-buttons">
        <a href="?view=unit" class="btn btn-outline-success float-left">Shto</a>
    </div>

    <table class="table table-responsive">
        <thead>
        <th>
            #
        </th>
        <th>
            Kodi
        </th>
        <th>
            Emri
        </th>
        <th width="5%">

        </th>
        <th width="5%">

        </th>
        </thead>

        <tbody>
        <?php
        $count = 1;
        foreach ($units as $unit) { ?>
            <tr>
                <td><?php echo $count ?></td>
                <td><?php echo $unit->code ?></td>
                <td><?php echo $unit->name ?></td>
                <td>
                    <a href="?view=unit&id=<?php echo $unit->id ?>" class="btn btn-outline-primary">Modifiko</a>
                </td>
                <td>
                    <a href="?view=units&task=delete&id=<?php echo $unit->id ?>" class="btn btn-outline-danger">Fshi</a>
                </td>
            </tr>
        <?php
        $count++;
        } ?>
        </tbody>
    </table>
</div>
