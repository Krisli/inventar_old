<?php
/**
 * Created by PhpStorm.
 * User: Niku
 * Date: 9/12/2018
 * Time: 9:05 AM
 */

require_once SITE_ROOT . DS . "entities" . DS . "items.php";

$entity = new ItemsEntity();
$items  = $entity->getItems();

if (isset($_GET['task'])) {
    if ($_GET['task'] == 'save') {
        $entity->saveItem($_POST);
    } else if ($_GET['task']  == 'delete') {
        $entity->deleteItem($_GET);
    }
}

?>

<div class="container">

    <div class="header-buttons">
        <a href="?view=item" class="btn btn-outline-success float-left">Shto</a>
    </div>

    <table class="table table-responsive">

    <thead>
    <th>
        #
    </th>
    <th>
        Emri
    </th>
    <th>
        Pershkrimi
    </th>
    <th>
        Data e krijimit
    </th>
    <th>
        Njesia
    </th>
    <th width="5%">

    </th>
    <th width="5%">

    </th>

    </thead>

    <tbody>

	<?php
    $count=1;
    foreach ($items as $item) { ?>
        <tr>
            <td><?php echo $count ?></td>
            <td><?php echo $item->name ?></td>
            <td><?php echo $item->description ?></td>
            <td><?php echo Util::formatDate($item->created) ?></td>
            <td><?php echo $item->unit_name ?></td>

            <td>
                <a href="?view=item&id=<?php echo $item->id ?>" class="btn btn-outline-primary">Modifiko</a>
            </td>
            <td>
                <a href="?view=items&task=delete&id=<?php echo $item->id ?>" class="btn btn-outline-danger">Fshi</a>
            </td>
        </tr>
	<?php
	$count++;
	} ?>
    </tbody>
</table>
