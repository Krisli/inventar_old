<?php
/**
 * Created by PhpStorm.
 * User: krisl
 * Date: 9/13/2018
 * Time: 10:22 PM
 */

require_once SITE_ROOT . DS . "entities" . DS . "items.php";

$entity = new ItemsEntity();
$items  = $entity->getItemsReport();
?>

<div class="container">
	<table class="table table-responsive">
		<thead>
		<th>
			#
		</th>
		<th>
			Emri
		</th>
		<th>
			Pershkrimi
		</th>
		<th>
			Data e modifikimt
		</th>
		<th>
			Data e futjes
		</th>
		<th width="5%">
			Gjendja
		</th>
		</thead>

		<tbody>

		<?php
		$count=1;
		foreach ($items as $item) { ?>
			<tr>
				<td><?php echo $count ?></td>
				<td><?php echo $item->name ?></td>
				<td><?php echo $item->description ?></td>
				<td><?php echo Util::formatDate($item->last_updated) ?></td>
				<td><?php echo Util::formatDate($item->first_updated) ?></td>
				<td><?php echo $item->amount.' '.$item->unit ?></td>
			</tr>
			<?php
			$count++;
		} ?>
		</tbody>
	</table>

