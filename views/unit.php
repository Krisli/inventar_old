<?php
/**
 * Created by PhpStorm.
 * User: krisl
 * Date: 9/12/2018
 * Time: 8:45 PM
 */

require_once SITE_ROOT . DS . "entities" . DS . "units.php";

$unit = null;
if (isset($_GET['id'])) {
	$entity = new UnitsEntity();
	$unit = $entity->getUnit($_GET['id']);
}

$name = !empty($unit) ? $unit->name : '';
$code = !empty($unit) ? $unit->code : '';
$id   = !empty($unit) ? $unit->id : 0;

?>

<div class="container">
	<form method="post" action="?view=units&task=save">
		<div class="form-group">
			<label for="name">Emer</label>
			<input type="text" id="name" name="name" placeholder="Emer" value="<?php echo $name ?>"/>
		</div>
		<div class="form-group">
			<label for="code">Kodi</label>
			<input type="text" id="code" name="code" placeholder="Kodi" value="<?php echo $code ?>"/>
		</div>
		<input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
		<button type="submit" class="btn btn-outline-success">Ruaj</button>
		<a href="?view=units" class="btn btn-outline-warning">Anullo</a>
	</form>
</div>