<?php
/**
 * Created by PhpStorm.
 * User: Niku
 * Date: 9/13/2018
 * Time: 2:08 PM
 */



require_once SITE_ROOT . DS . "entities" . DS . "items.php";
require_once SITE_ROOT . DS . "entities" . DS . "actions.php";

$entity2 = new ItemsEntity();
$items  = $entity2->getItems();

$action = null;
if (isset($_GET['id'])) {
    $entity = new ActionsEntity();
    $action = $entity->getAction($_GET['id']);
}

$outgoing = !empty($action) ? $action->outgoing : '';
$incoming = !empty($action) ? $action->incoming : '';
$id   = !empty($action) ? $action->id : 0;

?>

<div class="container">
    <form method="post" action="?view=actions&task=save">
        <div class="form-group">
            <label for="outgoing">Shitjet     </label>
            <input type="text" id="outgoing" name="outgoing" placeholder="Shitjet" value="<?php echo $outgoing ?>"/>
        </div>

        <div class="form-group">
            <label for="incoming">Furnizimet   </label>
            <input type="text" id="incoming" name="incoming" placeholder="Furnizimet" value="<?php echo $incoming ?>"/>
        </div>

        <div class="form-group">
            <label for="item">Produkti</label>
            <select name="item_id" id="item">
	            <?php
	            foreach ($items as $item) { ?>
                    <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
	            <?php } ?>
            </select>
        </div>

        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
        <button type="submit" class="btn btn-outline-success">Ruaj</button>
        <a href="?view=actions" class="btn btn-outline-warning">Anullo</a>
    </form>
</div>