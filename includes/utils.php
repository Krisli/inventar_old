<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 18-09-12
 * Time: 1.34.MD
 */

/**
 * Static Class Util
 *
 * Contains general functionality that may be used
 */
class Util
{
	/**
	 * Dummy constructor.
	 */
	private function __construct()
	{

	}

	/**
	 * Dummy clone.
	 */
	private function __clone()
	{

	}

	/**
	 * Method that returns the singleton instance
	 *
	 * @return Util
	 */
	public static function getInstance()
	{
		static $instance;
		if ($instance === null) {
			$instance = new Util();
		}
		return $instance;
	}

	/**
	 * Returns a mysqli connection object to the database
	 *
	 * @return mysqli
	 */
	public static function getDBConn()
	{
		$config = self::getDBConfig();

		// Create connection
		$conn = new mysqli($config->servername, $config->username, $config->password, $config->database);

		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		return $conn;
	}

	/**
	 * Retrieves the db config values from the xml configuration file
	 *
	 * @return SimpleXMLElement
	 */
	public static function getDBConfig()
	{
		// load db configuration from xml file
		$config = simplexml_load_file("db/config.xml") or die("Error: Cannot create object");

		return $config;
	}

	/**
	 * Redirects site to the specified view
	 *
	 * @param null $view string name of the view
	 */
	public static function redirect($view = null)
	{
		if (!empty($view)) {
			header('Location: ?view='.$view);
		}
	}

	/**
	 * Formats date to day-month-Year format
	 *
	 * @param $date string
	 *
	 * @return false|string
	 */
	public static function formatDate($date)
	{
		return !empty($date) ? date('d-m-Y', strtotime($date)) : '' ;
	}
}