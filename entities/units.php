<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 18-09-12
 * Time: 5.30.MD
 */

/**
 * Entity class of Units
 */
class UnitsEntity
{
	/**
	 * Get all units
	 *
	 * @return array
	 */
	function getUnits()
	{
		$conn   = Util::getDBConn();
		$query  = "SELECT * FROM units";
		$result = $conn->query($query);

		$units = array();
		if ($result) {
			while ($obj = $result->fetch_object()) {
				$units[] = $obj;
			}

			$result->close();
		}

		return $units;
	}

	/**
	 * @param $id int
	 *
	 * @return bool|null|object|stdClass
	 */
	function getUnit($id)
	{
		$conn = Util::getDBConn();

		if (empty($id)) {
			return false;
		}

		$id = $conn->real_escape_string($id);
		$query = "select *
                  from units
                  where id=" . $id;

		$result = $conn->query($query);

		$unit = null;
		if ($result) {
			$unit = $result->fetch_object();
		}

		$result->close();
		$conn->close();

		return $unit;
	}

	/**
	 * @param $data array
	 *
	 * @return bool
	 */
	function saveUnit($data)
	{
		if (!empty($data['id'])) {
			$this->updateUnit($data);
			return true;
		}

		$conn = Util::getDBConn();
		$name = $data['name'];
		$code = $data['code'];

		$sql = "INSERT INTO units(name, code) VALUES ('$name', '$code')";

		$result = $conn->query($sql);
		$conn->close();

		Util::redirect('units');

		return true;
	}

	/**
	 * @param $data array
	 *
	 */
	function updateUnit($data)
	{
		$conn = Util::getDBConn();
		$id   = $data['id'];
		$name = $data['name'];
		$code = $data['code'];

		$sql = "update units set name = '$name', code = '$code' where id = $id";

		$result = $conn->query($sql);
		$conn->close();

		Util::redirect('units');
	}

	/**
	 * @param $data array
	 *
	 * @return bool|mysqli_result
	 */
	function deleteUnit($data)
	{
		if (!isset($data['id'])) {
			return false;
		}

		$conn = Util::getDBConn();
		$id   = $data['id'];

		$sql = "delete from units where id = $id";

		$result = $conn->query($sql);
		$conn->close();

		Util::redirect('units');

		return true;
	}
}