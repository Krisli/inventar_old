<?php
/**
 * Created by PhpStorm.
 * User: Niku
 * Date: 18-09-12
 * Time: 5.30.MD
 */

/**
 * Entity class of Items
 */
class ItemsEntity
{
    /**
     * Get all items
     *
     * @return array
     */
    function getItems()
    {
        $conn   = Util::getDBConn();
        $query  = "select it.*, u.name as unit_name
                   from items as it
                   left join units as u on it.unit_id = u.id";
        $result = $conn->query($query);

        $items = array();
        if ($result) {
            while ($obj = $result->fetch_object()) {
                $items[] = $obj;
            }

            $result->close();
        }

        return $items;
    }

    function getUnit($id)
    {
        $conn = Util::getDBConn();

        if (empty($id)) {
            return false;
        }

        $id = $conn->real_escape_string($id);
        $query = "select *
                  from units
                  where id=" . $id;

        $result = $conn->query($query);

        $unit = null;
        if ($result) {
            $unit = $result->fetch_object();
        }

        $result->close();
        $conn->close();

        return $unit;
    }

    function getItem($id)
    {
        $conn = Util::getDBConn();

        if (empty($id)) {
            return false;
        }

        $id = $conn->real_escape_string($id);
        $query = "select *
                  from items
                  where id=" . $id;

        $result = $conn->query($query);

        $item = null;
        if ($result) {
            $item = $result->fetch_object();
        }

        $result->close();
        $conn->close();

        return $item;
    }

    function saveItem($data)
    {
        if (!empty($data['id'])) {
            $this->updateItem($data);
            return true;
        }

        $conn = Util::getDBConn();
        $name = $data['name'];
        $description = $data['description'];
        $unit_id = $data['unit_id'];


        $sql = "INSERT INTO items(name,description,created,unit_id) VALUES ('$name', '$description',now(),'$unit_id')";

        $result = $conn->query($sql);
        $conn->close();

        Util::redirect('items');

        return true;
    }

    function updateItem($data)
    {
        $conn = Util::getDBConn();
        $id   = $data['id'];
        $name = $data['name'];
        $description = $data['description'];
        $unit_id = $data['unit_id'];

        $sql = "update items set name = '$name', description = '$description',unit_id = '$unit_id' where id = $id";

        $result = $conn->query($sql);
        $conn->close();

        Util::redirect('items');
    }

    function deleteItem($data)
    {
        if (!isset($data['id'])) {
            return false;
        }

        $conn = Util::getDBConn();
        $id   = $data['id'];

        $sql = "delete from items where id = $id";

        $result = $conn->query($sql);
        $conn->close();

        Util::redirect('items');

        return true;
    }

    function getItemsReport()
    {
	    $conn = Util::getDBConn();

	    $query = "select it.name, it.description, MAX(a.date) as last_updated, MIN(a.date) as first_updated, u.code as unit,
				  (SUM(IFNULL(a.incoming, 0)) - SUM(IFNULL(a.outgoing, 0))) as amount
	              from items as it
	              left join units as u on it.unit_id = u.id
	              left join actions as a on it.id = a.item_id
	              group by it.id
	              order by it.name asc";

	    $result = $conn->query($query);

	    $items = array();
	    if ($result) {
		    while ($obj = $result->fetch_object()) {
			    $items[] = $obj;
		    }

		    $result->close();
	    }

	    return $items;
    }

}