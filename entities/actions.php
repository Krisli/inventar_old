<?php
/**
 * Created by PhpStorm.
 * User: Niku
 * Date: 9/13/2018
 * Time: 1:11 PM
 */



/**
 * Entity class of Actions
 */
class ActionsEntity
{
    /**
     * Get all actions
     *
     * @return array
     */
    function getActions()
    {
        $conn   = Util::getDBConn();
        $query  = "select ac.*, i.name as item_name
                   from actions as ac
                   left join items as i on ac.item_id = i.id
                   order by ac.id desc";
        $result = $conn->query($query);

        $actions = array();
        if ($result) {
            while ($obj = $result->fetch_object()) {
                $actions[] = $obj;
            }

            $result->close();
        }

        return $actions;
    }

    /**
     * @param $id int
     *
     * @return bool|null|object|stdClass
     */
    function getAction($id)
    {
        $conn = Util::getDBConn();

        if (empty($id)) {
            return false;
        }

        $id = $conn->real_escape_string($id);
        $query = "select *
                  from actions
                  where id=" . $id;

        $result = $conn->query($query);

        $action = null;
        if ($result) {
            $action = $result->fetch_object();
        }

        $result->close();
        $conn->close();

        return $action;
    }

    /**
     * @param $data array
     *
     * @return bool
     */
    function saveAction($data)
    {
        if (!empty($data['id'])) {
            $this->updateAction($data);
            return true;
        }

	    $conn     = Util::getDBConn();
	    $outgoing = (float)$data['outgoing'];
	    $incoming = (float)$data['incoming'];
	    $item_id  = (int)$data['item_id'];

	    $sql = "INSERT INTO actions(outgoing,incoming,date,item_id) VALUES ('$outgoing', '$incoming',now(),'$item_id')";

	    $result = $conn->query($sql);
	    $conn->close();

	    Util::redirect('actions');

	    return true;
    }

    /**
     * @param $data array
     *
     */
    function updateAction($data)
    {
	    $conn     = Util::getDBConn();
	    $id       = (int)$data['id'];
	    $outgoing = (float)$data['outgoing'];
	    $incoming = (float)$data['incoming'];
	    $item_id  = (int)$data['item_id'];

	    $sql = "update actions set outgoing = '$outgoing', incoming = '$incoming', item_id = $item_id  where id = $id";

	    $result = $conn->query($sql);
	    $conn->close();

        Util::redirect('actions');
    }

    /**
     * @param $data array
     *
     * @return bool|mysqli_result
     */
    function deleteAction($data)
    {
        if (!isset($data['id'])) {
            return false;
        }

        $conn = Util::getDBConn();
        $id   = $data['id'];

        $sql = "delete from actions where id = $id";

        $result = $conn->query($sql);
        $conn->close();

        Util::redirect('actions');

        return true;
    }
}