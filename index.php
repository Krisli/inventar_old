<?php
/**
 * Created by PhpStorm.
 * User: krisli
 * Date: 18-09-12
 * Time: 1.23.MD
 */

define('DS', DIRECTORY_SEPARATOR);
define('SITE_ROOT', __DIR__);

require_once "includes" . DS . "utils.php";
require_once "includes" . DS . "defines.php";

$view          = isset($_GET['view']) ? strtolower($_GET['view']) : "home";
$sidebarActive = isset($_GET['sidebar']) ? 1 : 0;
?>

<html>
<head>
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
            integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
            integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
            integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/general.css">
    <script src="assets/js/bootstrap.min.js"></script>
</head>

<body>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar" class="<?php echo $sidebarActive == 1 ? 'active' : '' ?>">
        <div class="sidebar-header">
            <h3>Bootstrap Sidebar</h3>
            <strong>BS</strong>
        </div>

        <ul class="list-unstyled components">
            <!-- Disabled/For later use

                <li class="active">
                 <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                     <i class="fas fa-home"></i>
                     Home
                 </a>

                 <ul class="collapse list-unstyled" id="homeSubmenu">
                     <li>
                         <a href="#">Home 1</a>
                     </li>
                     <li>
                         <a href="#">Home 2</a>
                     </li>
                     <li>
                         <a href="#">Home 3</a>
                     </li>
                 </ul>

            </li>
             -->
            <li class="<?php echo $view == 'report' ? 'active' : '' ?>">
                <a href="javascript:void(0)" onclick="changePage('?view=report')">
                    <i class="fas fa-briefcase"></i>
                    Inventari
                </a>
            </li>
            <li class="<?php echo $view == 'items' ? 'active' : '' ?>">
                <a href="javascript:void(0)" onclick="changePage('?view=items')">
                    <i class="fas fa-columns"></i>
                    Produktet
                </a>
            </li>
            <li class="<?php echo $view == 'actions' ? 'active' : '' ?>">
                <a href="javascript:void(0)" onclick="changePage('?view=actions')">
                    <i class="fas fa-cog"></i>
                    Veprimet
                </a>
            </li>
            <li class="<?php echo $view == 'units' ? 'active' : '' ?>">
                <a href="javascript:void(0)" onclick="changePage('?view=units')">
                    <i class="fas fa-ruler"></i>
                    Njesite
                </a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li>
                <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
            </li>
            <li>
                <a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a>
            </li>
        </ul>
    </nav>

    <!-- Page Content  -->
    <div id="content">
        <!-- Include views here -->
		<?php
		if (!file_exists("views" . DS . $view . ".php"))
		{
			$view = "404";
		}

		require_once "views" . DS . $view . ".php";
		?>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span>Toggle Sidebar</span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>
            </div>
        </nav>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });


    function changePage(page) {
        var sidebarActive = jQuery('#sidebar').hasClass('active');

        if (sidebarActive) {
            page += '&sidebar=1';
        }

        window.location.href = page;
    }
</script>

</body>

</html>